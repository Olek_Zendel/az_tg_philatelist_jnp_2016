#include <iostream>
#include <regex>
#include <string>
#include <vector>
#include <set>
#include <ctype.h>

using namespace std;
string lookahead(string q, string u) {
  return q+ "(?=" + u + ")";
}


const string INNER_SPACE_REGEX = " +";
int lineCounter=0,adding=1 , year1, year2; // adding used to indicate we are not querying
const string VALUE_REGEX = "([0-9]+(?:[,.][0-9]+)?)";

const string YEAR_REGEX = "([1-9]+[0-9]*)"; // we don't like trailing zeroes

const string POSTNAME_REGEX = "(.+)";
const string STAMP_NAME_REGEX = "(.+)";
const string STAMP_REGEX = "^"+ lookahead(STAMP_NAME_REGEX, lookahead(INNER_SPACE_REGEX , lookahead(VALUE_REGEX , lookahead(INNER_SPACE_REGEX, lookahead(YEAR_REGEX,  lookahead(INNER_SPACE_REGEX, POSTNAME_REGEX)))))) ;
const string QUERY_REGEX = "^"+ lookahead(YEAR_REGEX , lookahead(INNER_SPACE_REGEX , YEAR_REGEX));

const regex stampRegex(STAMP_REGEX);
const regex queryRegex(QUERY_REGEX);

/*
Sortowanie ma być kolejno po roku wydania znaczka, nazwie kraju lub poczty wydającej znaczek, wartości znaczka, nazwie znaczka. Liczby (rok wydania i wartość) sortujemy rosnąco według ich wartości. Napisy (pozostałe dwa pola) sortujemy leksykograficznie rosnąco - dla uproszczenia można przyjąć, że jest to porządek, jaki jest domyślne zdefiniowany na obiektach klasy string.
*/

/*
 * year
 * post/country name
 * value
 * stamp name
 */
typedef tuple<int, string, string, string> postStamp;

vector<postStamp> stamps;

int currentLine;


string normalizeValue(string value) {
  size_t f = value.find(",");

  //change "," to "."
  if (f != string::npos) {
    value = value.replace(f, string(",").length(), ".");
  }

  //strip zeros at the end
  if (value.find(".") != string::npos) {
    size_t i;
    for (i = value.length(); i --> 0 && value[i] == '0';);
    if (value[i] == '.') i--;
    value = value.substr(0, i+1);
  }

  return value;
}

bool comp(postStamp a, postStamp b) {
  if (get<0>(a) != get<0>(b)) {
    return get<0>(a) < get<0>(b);
  }
  if (get<1>(a) != get<1>(b)) {
    return get<1>(a).compare(get<1>(b)) < 0;
  }
  if (normalizeValue(get<2>(a)) != normalizeValue(get<2>(b))) {
    return normalizeValue(get<2>(a)).compare(normalizeValue(get<2>(b))) < 0;
  }
  if (get<3>(a) != get<3>(b)) {
    return get<3>(a).compare(get<3>(b)) < 0;
  }
  return false;
}

string removeMultipleSpaces(string str) {
  string ret = "";
  for (size_t i = 0; i < str.length(); i++) {
    if (i == 0 || (i > 0 && (str[i] != ' ' || str[i-1] != ' '))) {
      ret += str[i];
    }
  }
  return ret;
}

string stampToString(postStamp s) {
  string ret = "";
  ret = ret + to_string(get<0>(s)) + " " + get<1>(s) + " " + get<2>(s) + " " + get<3>(s);
  return removeMultipleSpaces(ret);
}

void query(int startYear, int endYear) {
  vector<postStamp>::iterator b = lower_bound(stamps.begin(), stamps.end(), make_tuple(startYear, "", "", "0"));
  vector<postStamp>::iterator e = lower_bound(stamps.begin(), stamps.end(), make_tuple(endYear+1, "", "", "0"));

  while (b != e) {
    cout << stampToString(*b) << "\n";
    b++;
  }
}

void addStamp(int year, string country, string value, string name) {
  stamps.push_back(make_tuple(year, country, value ,name));
}

string trim(string str) // from http://stackoverflow.com/questions/25829143/c-trim-whitespace-from-a-string
{
  size_t first = str.find_first_not_of(' ');
  size_t last = str.find_last_not_of(' ');
  return str.substr(first, (last-first+1));
}

void throwError(string line) {
  fprintf(stderr,"Error in line %d:%s\n",lineCounter,line.c_str());
}

string replaceWhitespaceWithSpace(string line) {
  for (int i=0 ; i< line.size(); i++) if (isspace(line[i])==true) line[i]=' ';
  return line;
}

void parseLine(string line) {
  lineCounter+=1;

  line = trim(replaceWhitespaceWithSpace(line));
  smatch matches;

  if (regex_search (line,matches,stampRegex)) {
    if (adding) {
      addStamp(stoi(matches[3]), matches[4], matches[2], matches[1]);

    } else throwError(line);
  } else {
    if (regex_search (line,matches,queryRegex)) {
      if (adding) {
        sort(stamps.begin(), stamps.end(), comp);
      }
      adding=0;
      year1 = stoi(matches[1]);
      year2 = stoi(matches[2]);

      if (year1<=year2) query(year1,year2);
      else throwError(line);
    } else {
      throwError(line);
    }
  }

}

int main() {
  string line;
  while (getline(cin, line)) {
    if (line=="") return 0;
      parseLine(line);
  }
  return 0;
}
